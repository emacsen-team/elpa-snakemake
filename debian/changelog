elpa-snakemake (2.0.0+git20231210.4ad41da-3) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 19:07:22 +0900

elpa-snakemake (2.0.0+git20231210.4ad41da-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.3.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 11:29:26 +0900

elpa-snakemake (2.0.0+git20231210.4ad41da-1) unstable; urgency=medium

  [ Xiyue Deng ]
  * Team upload
  * Sync to latest upstream snapshot (4ad41da) (Closes: #1068956)
  * Disable autopkgtest as the ERT tests require a writable $HOME
  * Modernize d/watch with substitute strings to be more robust
  * Add a version header in snakemake.el to workaround dh-elpa upstream
    version handling limitation
  * d/control: Trim trailing whitespace
  * Set upstream metadata fields: Repository
  * Update standards version to 4.7.0, no changes needed

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 15 Apr 2024 08:54:12 +0800

elpa-snakemake (2.0.0-3) unstable; urgency=medium

  * Add emacs-el >= 1:28 | elpa-transient Dependency to elpa-snakemake
    (Closes: #1024648)
  * Add elpa-snakemake-mode dependency to elpa-snakemake
    (Closes: #1025876)

 -- Diane Trout <diane@ghic.org>  Thu, 09 Feb 2023 15:22:12 -0800

elpa-snakemake (2.0.0-2) unstable; urgency=medium

  * Rebuild to create source only uploade

 -- Diane Trout <diane@ghic.org>  Mon, 19 Sep 2022 13:46:30 -0700

elpa-snakemake (2.0.0-1) unstable; urgency=medium

  * Initial release. (Closes: #1016769)

 -- Diane Trout <diane@ghic.org>  Sat, 6 Aug 2022 15:13:33 -0700
